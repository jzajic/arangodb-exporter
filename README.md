# arangodb-exporter

Prometheus exporter for ArangoDB.

Listens on port 9241. Configurable through the following environment
variables:

* `ARANGODB_SCHEME`: default `https`
* `ARANGODB_HOST`: default `localhost`
* `ARANGODB_PORT`: default `8529`
* `ARANGODB_USER`
* `ARANGODB_PASSWORD`
